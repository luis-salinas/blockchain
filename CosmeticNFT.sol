// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import 'https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC721/ERC721.sol';

contract CosmeticNFT is ERC721{
    uint256 token_count;
    address public admin;

    constructor()ERC721('Cosmetic NFT', 'CNFT'){
        admin = msg.sender;
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory){
        require(_exists(tokenId),'Token Inexistente');
        return 'DireccionDelNFT';
    }

    function mintNFT(address to) public {
        require(msg.sender==admin, 'Only Admin');
        token_count +=1;
        _mint(to,token_count);
    }

    function transferCosmetic (address to, uint256 tokenId) public {
        transferFrom(msg.sender, to, tokenId);
    }
}

