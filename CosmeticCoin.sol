// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";
contract CosmeticCoin is ERC20 {
    address public admin;

    constructor() ERC20('Cosmetic Coin', 'CCN'){
        _mint(msg.sender,10000000*10**18);
        admin = msg.sender;
    }

    function mint(address to, uint amount) external{
        require(msg.sender==admin, 'Only Admin');
        _mint(to,amount);
    }

    function burn(uint amount) external{
        require(msg.sender==admin, 'Only Admin');
        _burn(msg.sender,amount);
    }

    function transfer(address recipient, uint amount) public override returns (bool) {
        require(msg.sender==admin, 'Only Admin');
        _transfer(_msgSender(),recipient, amount);
        return true;
    }
    function transferFrom(address _from, address _to, uint256 amount) public override returns (bool) {
        require(msg.sender==admin, 'Only Admin');
        require(balanceOf(_from)>=amount, 'Insufficient balance');
        _transfer(_from,_to, amount);
        return true;
    }

    function transferPlayer(address recipient, uint amount) public returns (bool)  {
        require(amount<=1000);
        _transfer(_msgSender(), recipient, amount);
        return true;
    }
}